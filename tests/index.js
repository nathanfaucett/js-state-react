var tape = require("tape"),
	React = require("react"),
	Enzyme = require("enzyme"),
	EnzymeAdapter = require("enzyme-adapter-react-16"),
	State = require("@nathanfaucett/state"),
	inherits = require("@nathanfaucett/inherits"),
	deepEquals = require("@nathanfaucett/deep_equals"),
	jsdom = require("jsdom"),
	connect = require("..");

var dom = new jsdom.JSDOM("<!doctype html><html><body></body></html>");
global.window = dom.window;
global.document = dom.window.document;

var state = new State(),
	formStore = state.createStore("form", { text: "" }),
	RENDER_CALLED = 0,
	MAPPING_CALLED = 0,
	PREDICATE_CALLED = 0;

Enzyme.configure({ adapter: new EnzymeAdapter() });

function Text(props) {
	React.Component.call(this, props);
}
inherits(Text, React.Component);

Text.prototype.render = function() {
	RENDER_CALLED += 1;
	return React.createElement("p", null, this.props.form.text);
};

var ConnectedText = connect([formStore])(Text);

function Form(props) {
	React.Component.call(this, props);

	this.inputRef = React.createRef();
}
inherits(Form, React.Component);

Form.prototype.render = function() {
	RENDER_CALLED += 1;
	return React.createElement(
		"form",
		null,
		React.createElement("input", {
			onChange: function(e) {
				formStore.setState({
					text: e.target.value
				});
			},
			type: "text",
			ref: this.inputRef,
			value: this.props.form.text
		})
	);
};

var ConnectedForm = connect([formStore], {
	mapping: function mapping(nextState /*, nextProps */) {
		MAPPING_CALLED += 1;
		return nextState;
	},
	shouldUpdate: function shouldUpdate(prevState, nextState /*, props */) {
		PREDICATE_CALLED += 1;
		return !deepEquals(prevState, nextState);
	}
})(Form);

function Root(props) {
	React.Component.call(this, props);

	this.textRef = React.createRef();
	this.formRef = React.createRef();
}
inherits(Root, React.Component);

Root.prototype.render = function() {
	return React.createElement("div", null, [
		React.createElement(ConnectedText, {
			ref: this.textRef,
			key: "text"
		}),
		React.createElement(ConnectedForm, { ref: this.formRef, key: "form" })
	]);
};

tape("connect update", function(assert) {
	var wrapper = Enzyme.mount(React.createElement(Root));

	assert.equals(
		wrapper.instance().formRef.current.constructor.displayName,
		"Connect(Form)",
		"should wrap component name"
	);
	assert.equals(formStore.state().text, "", "store text should be empty");
	assert.equals(
		wrapper.instance().formRef.current.componentRef.current.props.form.text,
		"",
		"component props should reflect stores"
	);

	wrapper.find("input").simulate("change", { target: { value: "text" } });

	// wait for process.nextTick to finish
	setTimeout(function() {
		assert.equals(
			formStore.state().text,
			"text",
			"store's value should update"
		);
		assert.equals(
			wrapper.instance().formRef.current.componentRef.current.props.form
				.text,
			"text",
			"text should update to new store's value"
		);

		wrapper.find("input").simulate("change", { target: { value: "text" } });

		setTimeout(function() {
			assert.equals(
				formStore.state().text,
				"text",
				"store's text should not have changed"
			);
			assert.equals(
				wrapper.instance().formRef.current.componentRef.current.props
					.form.text,
				"text",
				"text should not have changed"
			);
			wrapper.unmount();

			assert.equals(
				PREDICATE_CALLED,
				2,
				"predicate should have been called"
			);
			assert.equals(MAPPING_CALLED, 2, "mapping should have been called");
			assert.equals(RENDER_CALLED, 4, "render should have been called");

			assert.end();
		});
	});
});
