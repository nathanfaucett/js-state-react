var React = require("react"),
    isArray = require("@nathanfaucett/is_array"),
    isFunction = require("@nathanfaucett/is_function"),
    arrayForEach = require("@nathanfaucett/array-for_each"),
    inherits = require("@nathanfaucett/inherits"),
    extend = require("@nathanfaucett/extend"),
    defaultMapping = require("./mapping"),
    defaultShouldUpdate = require("./shouldUpdate");

module.exports = connect;

function connect(stores, options) {
    var mapping, shouldUpdate;

    options = options || {};

    if (!isArray(stores)) {
        throw new TypeError(
            "connect(stores[, mapping[, shouldUpdate]]) stores must be an Array"
        );
    }

    mapping = isFunction(options.mapping) ? options.mapping : defaultMapping;
    shouldUpdate = isFunction(options.shouldUpdate)
        ? options.shouldUpdate
        : defaultShouldUpdate;

    function getNextState() {
        var state = {};

        arrayForEach(stores, function each(store) {
            state[store.name()] = store.state();
        });

        return state;
    }

    return function connectComponent(Component) {
        var ConnectPrototype;

        function Connect(props) {
            var _this = this;

            React.Component.call(this, props);

            this.componentRef = React.createRef();
            this._state = getNextState();
            this._updating = false;
            this._isMounted = false;

            this._onUpdate = function onUpdate() {
                if (!_this._updating) {
                    _this._updating = true;

                    process.nextTick(function onNextTick() {
                        _this._updating = false;
                        _this._updateIfNeeded(getNextState());
                    });
                }
            };

            arrayForEach(stores, function each(store) {
                store.on("update", _this._onUpdate);
            });
        }
        inherits(Connect, React.Component);
        ConnectPrototype = Connect.prototype;

        Connect.displayName =
            "Connect(" +
            (Component.displayName || Component.name || "Component") +
            ")";

        ConnectPrototype._updateIfNeeded = function(nextState) {
            if (this._isMounted) {
                if (
                    shouldUpdate.call(
                        this.componentRef.current,
                        this._state,
                        nextState,
                        this.props
                    ) !== false
                ) {
                    this._state = nextState;
                    this.forceUpdate();
                }
            }
        };

        ConnectPrototype.componentDidMount = function() {
            this._isMounted = true;
        };

        ConnectPrototype.componentWillUnmount = function() {
            var _this = this;

            arrayForEach(stores, function each(store) {
                store.off("update", _this._onUpdate);
            });

            this._isMounted = false;
        };

        ConnectPrototype.render = function() {
            return React.createElement(
                Component,
                extend(
                    { ref: this.componentRef },
                    this.props,
                    mapping(this._state, this.props)
                )
            );
        };

        return Connect;
    };
}
