var connect = require("./connect"),
	defaultMapping = require("./mapping"),
	defaultShouldUpdate = require("./shouldUpdate");

connect.defaultMapping = defaultMapping;
connect.defaultShouldUpdate = defaultShouldUpdate;

module.exports = connect;
