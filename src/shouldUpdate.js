var deepEquals = require("@nathanfaucett/deep_equals");

module.exports = shouldUpdate;

function shouldUpdate(prevState, nextState) {
	return !deepEquals(prevState, nextState);
}
