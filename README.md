# js-state-react

connect react components with state stores

## State Stores

```javascript
import State from "@nathanfaucett/state";

const state = new State();

let ID = 0;

const todos = state.createStore("todos", {
    list: []
});

todos.create = text => {
    const id = ID++;

    todos.updateState(state => {
        const list = state.list.slice();

        list.push({
            id: id,
            text: text
        });

        return {
            list: list
        };
    });
};

todos.remove = id => {
    todos.updateState(state => {
        const index = list.findIndex(todo => todo.id === id);

        let list = state.list;

        if (index !== -1) {
            list = list.slice();
            list.slice(index, 1);
        }

        return {
            list: list
        };
    });
};
```

## Components

```javascript
import React from "react";
import connect from "@nathanfaucett/state-react";
import state from "./state";
import todos from "./stores/todos";

// create store for form input
let todoListForm = state.createStore("todoListForm", { text: "" });

class TodoList extends React.Component {
    constructor(props) {
        super(props);

        this.onSubmit = e => {
            e.preventDefault();
            todos.create(this.props.todoListForm.text);
            todoListForm.setState({ text: "" });
        };

        this.onChange = e => {
            todoListForm.setState({ text: e.target.value });
        };
    }
    render() {
        return (
            <div class="TodoList">
                <form onSubmit={this.onSubmit}>
                    <input
                        value={this.props.todoListForm.text}
                        onChange={this.onChange}
                    />
                </form>
                {this.props.todos.list.map(todo => (
                    <p key={todo.id}>{todo.text}</p>
                ))}
            </div>
        );
    }
}

// update TodoList when todos or todoListForm emit an update
// on update adds todos state and todoListForm state to props
export default connect([todos, todoListForm], {
    mapping: (nextState, nextProps, props) => {
        // called on render should extract values from nextState to pass to component props
    },
    shouldUpdate: (prevState, nextState) => {
        // called on update, if returns false cancels update
    }
})(TodoList);
```
